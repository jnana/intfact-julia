#include <iostream>
#include <string.h>
#include <string>
#include <vector>
#include <gmp.h>
#include <stdlib.h>
#include <algorithm>
//#include "zeros1.hpp"
#include "zeros.hpp"
#include "primes.hpp"
#include <math.h>
#include <mpreal.h>

using namespace std;
using namespace mpfr;

#define BIN 2
#define PREC 2048

void print(vector<int> v) {
	for (int i = 0; i < v.size(); ++i) {
		printf("%d\t, ", v[i]);
	}
	cout << "\n";
}


void print(vector<char*> v) {
	for (int i = 0; i < v.size(); ++i) {
		printf("%s\t, ", v[i]);
	}
	cout << "\n";
}


void print(vector<long double> v) {
	for (int i = 0; i < v.size(); ++i) {
		printf("%Lf\t, ", v[i]);
	}
	cout << "\n";
}

char* convert(char* num, int b) {
	std::string ss = "";
	mpz_t r;
	mpz_init(r);
	mpz_t n;
	mpz_init(n);
	mpz_set_str(n, num, 10);
	while (mpz_cmp_si(n, 0) != 0) {
		mpz_mod_ui(r, n, b);
		mpz_div_ui(n, n, b);
		char* rs = mpz_get_str(0, 10, r);
		ss = ss + strdup(rs);
	}
	mpz_clear(n);
	mpz_clear(r);
	std::reverse(ss.begin(), ss.end());
	return strdup(ss.c_str());
}

void partition(char* bnum, vector<char*>& bv) {
	int cnt = 0;
	int l = strlen(bnum);
	std::string ss = "";
	while (cnt < l) {
		int bk = bnum[cnt] - '0';
		int lookahead = -1;
		if (cnt < (l-1)) {
			lookahead = bnum[cnt+1] - '0';
		}
		if  (lookahead == 0) {
			if (ss.size() > 0) {
				bv.push_back(strdup((char*)ss.c_str()));
			}
			ss = bnum[cnt];
			++cnt;
			if (cnt < l) {
				ss = ss + bnum[cnt];
			}
			++cnt;
			while (lookahead != 1 && cnt < l) {
				lookahead= bnum[cnt]-'0';
				ss= ss + bnum[cnt];
				++cnt;
			}
			bv.push_back(strdup((char*)ss.c_str()));
			ss = "";
			if (cnt >= l) {
				return;
			}
		} else {
			ss = ss + bnum[cnt];
			++cnt;                 
		}
	}
	if (ss.size() > 0) {
		bv.push_back(strdup((char*)ss.c_str()));
	}
}

void summation(vector<char*> bv, vector<char*>& sums) {
	int top_count = 0;
	mpreal sum1 = 0.0;
	for (int i = 0; i < bv.size(); ++i) {
		char* bvec = bv[i];
		for (int j = 0; j < strlen(bvec);++j) {
			int bk = bvec[j]-'0';
			if (bk == 1) {
				sum1 += mpreal(zeros[top_count]);
			} else {
				sum1 -= mpreal(zeros[top_count]);
			}
			++top_count;
		}
		std::string ss = sum1.toString();
		sums.push_back(strdup(ss.c_str()));
		sum1 = 0.0;
	}
	return;
}

void strip_int(vector<char*> sums, vector<char*>& partition_sum) {
	for (int i = 0; i < sums.size(); ++i) {
		char* val = sums[i];
		bool neg = false;
		if (val[0] == '-') {
			neg = true;
		}
		char* ptr = strchr(val, '.');
		val = ptr;
		std::string ss = "0";
		ss = ss + val;
		partition_sum.push_back(strdup(ss.c_str()));
	}
}

void generate_mask(vector<char*> partition_sum, vector<int>& mask1, vector<int>& mask2) {
	for (int i = 0; i < partition_sum.size(); ++i) {
		char* val = partition_sum[i]; 
		if (val[0]=='-') {
			mask1.push_back(0);
			mask2.push_back(1);
		} else {
			mask1.push_back(1);
			mask2.push_back(0);
		}
	}
	return;
}

void partition_and_sum(char* bnum, vector<char*>& partition_sum, vector<int>& mask1, vector<int>& mask2) {
	vector<char*> bv;
	partition(bnum, bv);
	//print(bv);
	vector<char*> sums;
	summation(bv, sums);
	//print(sums);
	generate_mask(sums, mask1, mask2);
	strip_int(sums, partition_sum);
	return; 
}

bool  match(vector<int> mask_vector,vector<int>  mask1, int sense ) {
	bool _sense = true;
	for (int i = 0; i < mask1.size(); ++i ) {
		int m1 = mask_vector[i];
		int m2 = mask1[i];
		if (sense == 1 && m1 != m2) {
			_sense = false;
			return _sense;
		} else if (sense == 0 && m1 == m2) {
			_sense = false;
			return _sense;
		}
	}
	return _sense;
}

std::string product(vector<char*> row_vector) {
	mpreal sum = 1.0;
	for (int i = 0; i < row_vector.size(); ++i) {
		mpreal row = row_vector[i];
		sum *= row;
	}   
	cout << "Product: " <<sum<<"\n";
	return std::string(sum.toString());
}

std::string sum_row(vector<char*> row_vector) {
	mpreal sum = 0.0;
	for (int i = 0; i < row_vector.size(); ++i) {
		mpreal row = row_vector[i];
		mpreal intpart = 0;
		mpreal fracpart = mpfr::modf(row, intpart);
		sum += fracpart;
	}   
	cout << "Sum: " <<sum<<"\n";
	return std::string(sum.toString());
}

const int* index_primes(int p) {
	return std::find(primes, primes+100000,  p);
}

void prune(vector<char*>& _ps, vector<char*> ps, int i) {
	for (int j = 0; j < ps.size(); ++j) {
		 char* ss = strdup(ps[j]);
		 char* ptr = strchr(ss, '.');
		 char* vs = ptr+i+1;
		 vs[0] = '\0';
	         _ps.push_back(ss);
	}
        print(_ps);
        cin.get();
	return;
}

void run_state_machine(vector<char*> partition_sum, const vector<int> mask1, const  vector<int> mask2, vector<int> factor_posits, int i) {
	mpreal::set_default_prec(i+1);
	vector< vector<char*> > result_matrix;
	vector< vector<int> > mask_result_matrix;
	vector<char*> _partition_sum;
	prune(_partition_sum, partition_sum, i);
	print(_partition_sum);
	result_matrix.push_back(_partition_sum);
	mask_result_matrix.push_back(mask1);
	int pos = 0;
	while (!iszero(mpreal(product(_partition_sum)) )) {
		vector<char*>* row_vector = new vector<char*>();
		vector<int> mask_vector;
		for (int i = 0; i < _partition_sum.size(); ++i) {
			mpreal val = _partition_sum[i];
			mpreal intpart = 0;
			mpreal fracpart = mpfr::modf(val, intpart);
			fracpart *= 2;
			val = fracpart;
			fracpart = mpfr::modf(val, intpart);
			if (intpart == mpreal(1)) {
				mask_vector.push_back(1);
			} else {
				mask_vector.push_back(0);
			}
			std::string ss = val.toString();
			row_vector->push_back(strdup((char*)ss.c_str()));
		}
		++pos;
                const int* p = 0;
		cout << "@Pos:\t"<<pos<<"\tResult: \t"<<sum<<"\n";
		print(*row_vector);
		if ((p=index_primes(pos))!= (primes +100000)) {
			cout << "\n@Pos: "<<pos<<"\n";
			bool match_result = match(mask_vector, mask1, 1);
			bool _match_result = match(mask_vector, mask2, 0);
			if (match_result) {
				cout << "Match @Pos:\t"<<pos<<"\tResult: \t"<<sum_row(*row_vector)<<"\n";
				print(*row_vector);
				cin.get();
			} else if (_match_result) {
				cout << "Match- @Pos:\t"<<pos<<"\tResult: \t"<<sum_row(*row_vector)<<"\n";
				print(*row_vector);
				cin.get();
			}
		}
		result_matrix.push_back(*row_vector);
		_partition_sum.clear();
		for (int i = 0; i < row_vector->size(); ++i) {
			_partition_sum.push_back(row_vector->at(i));
		}
	}

}

int main(int argc, char* argv[]) {
	std::string s = argv[1];
	char* num = strdup(s.c_str());
	printf("Number Entered was : %s\n", num);
	char* bnum = convert(num, BIN);
	//	printf("Binary Representation: %s\n", bnum);
	vector<char*> partition_sum;
	vector<int> mask1;
	vector<int> mask2;
	partition_and_sum(bnum, partition_sum, mask1, mask2);  
	////print(partition_sum);
	//print(mask1);
	//print(mask2);
	vector<int> factor_posits;
	int l= strlen(bnum);
	for (int i = 1; i <= l; ++i) {
		run_state_machine(partition_sum, mask1, mask2, factor_posits, i);
		cout <<"\nEnd of Iteration: \t"<<i<<"\n";
		cin.get();
	}
	//print(factor_posits);*/
	return 0;
}
